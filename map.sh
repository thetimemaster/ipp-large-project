#!/usr/bin/env bash

if test -f "${1}"; then
    i=2;
    while test -n "${!i}"; do
        if (test "${!i}" -gt 0 2> /dev/null) && (test "${!i}" -lt 1000 2> /dev/null); then
            line=$(cat "${1}" | grep "^${!i};");
            if test -n "${line}"; then
                IFS=';';
                read -ra words <<< "${line}";
                IFS=' ';

                sum=0;
                for j in "${!words[@]}"
                do
                    if test $((${j} % 3)) -eq 2
                    then
                        sum=$((${sum} + ${words[${j}]}));
                    fi
                done
                echo "${!i};${sum}"
             fi
        else
            exit 1;
        fi
        ((i++));
    done

    if test "${i}" -gt 2; then
        exit 0;
    else
        exit 1;
    fi
else
    exit 1;
fi