/** @file
 * @brief Implementacja zrównoważonego drzewa binarnego.
 * Implementacja drzewa binarneogo TREAP równoważonego lowowymi wagami
 * ustawionymi w kopiec. Używa kluczay typu int64_t przechowujce
 * wskaźniki na dane będące podtypami BSTData.
 *
 * @author Piotr Kowalewski <pk406605@students.mimuw.edu.pl>
 */
#include <stdlib.h>
#include <assert.h>
#include "bst.h"
#include <stdio.h>
#include "map.h"

/** @brief Lewa rotacja BST
 * @param[in,out] node – wskaźnik na korzeń rotowanego podrzewa
 * @return Nowy korzeń rotowanego poddrzewa
 */
static BSTNode* leftRotation(BSTNode* node)
{
	BSTNode* rson = node->rson;
	node->rson=rson->lson;
	rson->lson = node;
	return rson;
}

/** @brief Prawa rotacja BST
 * @param[in,out] node – wskaźnik na korzeń rotowanego podrzewa
 * @return Nowy korzeń rotowanego poddrzewa
 */
static BSTNode* rightRotation(BSTNode* node)
{
	BSTNode* lson = node->lson;
	node->lson=lson->rson;
	lson->rson = node;
	return lson;
}

/** @brief Konstruktor węzła BST
 * Tworzy pusty węzeł BST z losową wagą do stosu.
 * @return Wskaźnik na nowy obiekt BSTNode lub NULL jeśli nie udało się zaalokować pamięci.
 */
static BSTNode* newNode()
{
	BSTNode* node = malloc(sizeof(BSTNode));
	if(node == NULL) return NULL;
	node->lson = NULL;
	node->rson = NULL;
	node->data = NULL;
	node->key = 0;
	node->treapWeight = rand();
	return node;
}

BSTData* newCityData(char* name, int64_t hash)
{

	BSTData* cityData = malloc(sizeof(BSTData));
	if(cityData == NULL) return NULL;
	cityData->city.roads = NULL;
	cityData->city.routes = NULL;
	cityData->city.name = name;
	cityData->city.hash = hash;
	return cityData;
}

BSTData* newRoadData(unsigned length, int year)
{
	BSTData* roadData = malloc(sizeof(BSTData));
	if(roadData == NULL) return NULL;
	roadData->road.length = length;
	roadData->road.routes = NULL;
	roadData->road.year = year;
	roadData->road.refs = 2;
	return roadData;
}

BSTData* newRouteData(unsigned id, BSTNode* start, BSTNode* end)
{
	BSTData* routeData = malloc(sizeof(BSTData));
	if(routeData == NULL) return NULL;
	routeData->route.id = id;
	routeData->route.start = start;
	routeData->route.end = end;
	return routeData;
}

BSTData* newDxData(int year, int year2, int64_t distance, BSTNode* from)
{
	BSTData* dxData = malloc(sizeof(BSTData));
	if(dxData == NULL) return NULL;
	dxData->dx.distance = distance;
	dxData->dx.from  = from;
	dxData->dx.year  = year;
	dxData->dx.year2 = year2;
	return dxData;
}

BSTData* newPqData(BSTNode* childTree)
{
	BSTData* pqData = malloc(sizeof(BSTData));
	if(pqData == NULL) return NULL;
	pqData->pq.childTree = childTree;
	return pqData;
}

BSTNode* bstFind(int64_t key, BSTNode* in)
{
	if(in == NULL) return NULL;
	if(key == in->key) return in;

	if(key < in->key) return bstFind(key, in->lson);
	return bstFind(key, in->rson);
}

BSTNode* bstMin(BSTNode* in)
{
	if(in == NULL) return NULL;

	while(in->lson != NULL) in = in->lson;

	return in;
}

BSTNode* bstMax(BSTNode* in)
{
	if(in == NULL) return NULL;

	while(in->rson != NULL) in = in->rson;

	return in;
}

BSTNode* bstInsert(int64_t key, BSTData* data, BSTNode* in, int* flags)
{
	if(in == NULL)
	{
		in = newNode();
		if(in == NULL)
		{
			*flags |= MALLOC_ERROR;
			return NULL;
		}

		in->key = key;
		in->data = data;
		return in;
	}

	//implementacja drzewa może mieć problemy ze zwalnianiem
	//obiektów BSTData nadpisanych pzez nowy obiekt, zakładamy że
	//nie można dodawać istniejącego klucza do drzewa
	assert(key != in->key);


	if(key < in->key)
	{
		in->lson = bstInsert(key, data, in->lson, flags);
		if(in->lson == NULL) return in;
		if(in->lson->treapWeight < in->treapWeight) in = rightRotation(in);
		return in;
	}

	else
	{
		in->rson = bstInsert(key, data, in->rson, flags);
		if(in->rson == NULL) return in;
		if(in->rson->treapWeight < in->treapWeight) in = leftRotation(in);
		return in;
	}
}

/** @brief Łączy dwa drzewa BST w jedno
 * Wszystkie klucze w lewym BST muszą być ściśle mniejsze niż wszystkie w prawym
 * @param[in] left  – wskaźnik na lewe BST
 * @param[in] right – wskaźnik na prawe BST
 * @return Wskaźnik na połączone BST. Left i right przestają istnieć jako osobne
 */
static BSTNode* merge(BSTNode* left, BSTNode* right)
{
	if(left == NULL) return right;
	if(right == NULL) return left;

	if(left->treapWeight < right->treapWeight)
	{
		left->rson = merge(left->rson, right);
		return left;
	}
	else
	{
		right->lson = merge(left, right->lson);
		return right;
	}
}

BSTNode* bstRemove(int64_t key, BSTNode* in, bool freeData)
{
	if(in == NULL) return NULL;
	if(key == in->key)
	{
		BSTNode* node = merge(in->lson, in->rson);
		if(freeData) free(in->data);
		free(in);
		return node;
	}

	if(key < in->key) in->lson = bstRemove(key, in->lson, freeData);
	else in-> rson = bstRemove(key, in->rson, freeData);

	return in;
}

BSTNode** bstToArray(BSTNode* node, int* outSize)
{
	if(node == NULL)
	{
		BSTNode** array = malloc(sizeof(BSTNode*));
		if(array == NULL) return NULL;

		array[0] = NULL;
		*outSize = 1;
		return array;
	}

	int leftSize, rightSize;

	BSTNode** left = bstToArray(node->lson, &leftSize);
	if(left == NULL) return NULL;

	BSTNode** right = bstToArray(node->rson, &rightSize);
	if(right == NULL)
	{
		free(left);
		return NULL;
	}

	BSTNode** all = malloc(sizeof(BSTNode*) * (leftSize + rightSize));
	if(all == NULL)
	{
		free(left);
		free(right);
		return NULL;
	}

	for(int i = 0; i < leftSize; i++) all[i] = left[i];
	for(int i = 0; i < rightSize; i++) all[leftSize + i] = right[i];
	all[leftSize - 1] = node;
	*outSize = leftSize + rightSize;

	free(left);
	free(right);
	return all;
}


void freeBST(BSTNode* node, int dataType)
{
	if(node == NULL) return;

	freeBST(node->lson, dataType);
	freeBST(node->rson, dataType);

	if(dataType == CITY_DATA)
	{
		freeBST(node->data->city.roads, ROAD_DATA);
		freeBST(node->data->city.routes, DONT_FREE);
		free((void *)node->data->city.name);
		free(node->data);
	}

	if(dataType == DX_DATA)
	{
		free(node->data);
	}

	if(dataType == ROAD_DATA)
	{
		node->data->road.refs--;
		if(node->data->road.refs == 0)
		{
			freeBST(node->data->road.routes, DONT_FREE);
			free(node->data);
		}
	}

	if(dataType == ROUTE_DATA)
	{
		free(node->data);
	}

	free(node);
}
