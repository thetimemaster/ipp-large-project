/** @file
 * Interfejs metod tekstowych dla Mapy
 *
 * @author Piotr Kowalewski <pk406605@students.mimuw.edu.pl>
 */


#ifndef __TEXT_H__
#define __TEXT_H__

#include <inttypes.h>

/** @brief Oblicza hasz napisu, nazwy miasta
 * @param[in] str           – wskaźnik na nazwę miasta
 * @return Obliczony hasz nazwy miasta
 */
int64_t getHash(const char* str);

/** @brief Zwraca pusty napis
 * @return Wskaźnik na pusty napis lub NULL gdy nie uda się zaalokować pamięci
 */
char* emptyString();

/** @brief Kopiuje stringa
 * @param[in] name    – wskaźnik na napis do skopiowania
 * @return Wskaźnik na skopiowany napis lub NULL gdy nie uda się zaalokować pamięci
 */
char* copyName(char* name);

/** @brief Dopisuje text do opisu
 * @param[in] desc           – wskaźnik na opis
 * @param[in] text           – wskaźnik na napis do dodania
 * @param[in,out] length     – aktualna długość opisu
 * @param[in,out] buffLength – aktualna długość zaalokowanego miejsca na opis
 * @return Wskaźnik na opis (string) z dopisanym text po średniku lub NULL
 * jeśli nie uda się zalokować pamięci
 */
char* appendText(char* desc, char* text, int* length, int* buffLength);

/** @brief Dopisuje liczbę do opisu
 * @param[in] desc           – wskaźnik na opis
 * @param[in] number         – liczba do dodania
 * @param[in,out] length     – aktualna długość opisu
 * @param[in,out] buffLength – aktualna długość zaalokowanego miejsca na opis
 * @return Wskaźnik na opis (string) z dopisaną liczbą po średniku lub NULL
 * jeśli nie uda się zalokować pamięci
 */
char* appendNumber(char* desc, int number, int* length, int* buffLength);

#endif
