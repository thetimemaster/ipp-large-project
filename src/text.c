/** @file
 * Implementacja metod tekstowych dla Mapy
 *
 * @author Piotr Kowalewski <pk406605@students.mimuw.edu.pl>
 */
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "text.h"
#include "map.h"

int64_t getHash(const char* str)
{
	const int mod1 = 1000000007;
	const int mod2 = 1000000009;
	const int base1 = 257;
	const int base2 = 273;

	if(str == NULL) return INVALID_DATA;

	int64_t hash1 = 0;
	int64_t hash2 = 0;

	int i = 0;

	while(str[i] != '\0')
	{
		if((str[i] >= 0 && str[i] <= 31) || str[i] == ';') return INVALID_DATA;
		hash1 = (hash1 * base1 + str[i]) % mod1;
		hash2 = (hash2 * base2 + str[i]) % mod2;
		i++;
	}

	if(i == 0) return INVALID_DATA;

	return hash1 * mod2 + hash2;
}

char* copyName(char* name)
{
	int n = 0;
	while(name[n] != '\0') n++;

	char* o = malloc(sizeof(char) * (n + 1));
	if(o == NULL) return NULL;
	for(int i = 0; i <= n; i++) o[i] = name[i];

	return o;
}

char* emptyString()
{
	char* s = malloc(sizeof(char));
	if(s == NULL) return NULL;

	s[0] = '\0';
	return s;
}

char* appendText(char* desc, char* text, int* length, int* buffLength)
{
	if(desc == NULL) return NULL;

	int i = 0;
	while(true)
	{
		if(*length + 1 == *buffLength)
		{
			char* newBuff = malloc(sizeof(char) * (*buffLength) * 2);
			if(newBuff == NULL) return NULL;

			for(int i = 0; i < *buffLength; i++) newBuff[i] = desc[i];
			*buffLength = (*buffLength) * 2;
			char* old = desc;
			desc = newBuff;
			free(old);
		}
		desc[*length] = text[i];
		desc[*length + 1] = '\0';

		if(text[i] == '\0')
		{
			desc[*length] = ';';
			(*length)++;
			break;
		}

		i++;
		(*length)++;
	}


	return desc;
}

char* appendNumber(char* desc, int number, int* length, int* buffLength)
{
	if(desc == NULL) return NULL;

	char* num = malloc(sizeof(char) * 15);
	if(num == NULL) return NULL;

	sprintf(num, "%d", number);
	if(num == NULL)
	{
		free(desc);
		return NULL;
	}

	desc = appendText(desc, num, length, buffLength);
	free(num);

	return desc;
}
