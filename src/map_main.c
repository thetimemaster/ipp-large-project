#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "text.h"
#include "parser.h"
#include "map.h"

#undef NDEBUG

int main() {
    Map* map = newMap();

    while(true)
    {
        InputData* data = parseLine(map);

        if(data->lineType == END_LINE)
        {
            freeInputData(data);
            break;
        }

        if(data->lineType == ADD_ROAD_LINE)
        {
            if(!addRoad(map, data->cities[0], data-> cities[1], data->lengths[0], data->years[0])) SignalError();
        }

        if(data->lineType == REPAIR_ROAD_LINE)
        {
            if(!repairRoad(map, data->cities[0], data-> cities[1], data->years[0])) SignalError();
        }

        if(data->lineType == ADD_ROUTE_LINE)
        {
            if(!newRouteExact(map, data->routeId, data->cities, data->years, data->lengths, data->inputSize)) SignalError();
        }

        if(data->lineType == ROUTE_DESCRIPTION_LINE)
        {
            const char* output = getRouteDescription(map, data->routeId);
            if(output == NULL) SignalError();
            else printf("%s\n",output);
            free((void *)output);
        }

        if(data->lineType == NEW_ROUTE_LINE)
        {
            if(!newRoute(map, data->routeId, data->cities[0], data-> cities[1])) SignalError();
        }

        if(data->lineType == EXTEND_ROUTE_LINE)
        {
            if(!extendRoute(map, data->routeId, data->cities[0])) SignalError();
        }

        if(data->lineType == REMOVE_ROAD_LINE)
        {
            if(!removeRoad(map, data->cities[0], data->cities[1])) SignalError();
        }

        if(data->lineType == REMOVE_ROUTE_LINE)
        {
            if(!removeRoute(map, data->routeId)) SignalError();
        }

        freeInputData(data);
    }

    deleteMap(map);

    return 0;
}
