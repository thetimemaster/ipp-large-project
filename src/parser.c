/** @file
 * @brief Implementacja wczytywania danych z wejścia
 * @author Piotr Kowalewski <pk406605@students.mimuw.edu.pl>
 */

#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "parser.h"
#include "map.h"
#include "text.h"

int lineNo = 0; ///< Globalna zmienna: numer wczytywanej linii

/** @brief Tworzy nową pustą strukturę InputData
 * Wszystkie tablice mają wielkość inputSize oraz są zerowane
 * @param[in] type      – typ wczytanej linii, jeden z \#defineów XYZ_LINE
 * @param[in] inputSize – wielkość tablic w strukturze
 * @return Wskaźnik na nowe InputData lub NULL gdy nie uda się
 * zaalokowac pamięci
 */
InputData* newInputData(int type, unsigned inputSize)
{
    InputData* data = malloc(sizeof(InputData));
    if(data == NULL) return NULL;

    data->cities = malloc(sizeof(char*) * inputSize);
    data->years = malloc(sizeof(int) * inputSize);
    data->lengths = malloc(sizeof(unsigned) * inputSize);

    if(data->cities == NULL || data->years == NULL || data->years == NULL)
    {
        free(data->cities);
        free(data->years);
        free(data->lengths);
        free(data);
        return NULL;
    }

    for(unsigned i = 0; i < inputSize; i++)
    {
        data->cities[i] = NULL;
        data->years[i] = 0;
        data->lengths[i] = 0;
    }

    data->inputSize = inputSize;
    data->routeId = 0;
    data->lineType = type;

    return data;
}

void freeInputData(InputData* data)
{
    if(data == NULL) return;

    for(int i = 0; i < data->inputSize; i++)
    {
        if(data->cities[i] != NULL) free(data->cities[i]);
    }
    free(data->cities);
    free(data->years);
    free(data->lengths);

    free(data);
}


/** @brief Wczytuje linię podzieloną średnikami
 * W przypadku problemów z alokazją pamięci na wczytane dane zwalnia
 * przekazaną strukturę map i kończy program z kodem 0.
 * @param[in,out] map       – wskaźnik na mapę do ewentualnego zwolnienia
 * @param[out] segmentCount – liczba segmentów oddzielonych średnikami, 0 <=> koniec wejścia
 * @return Wskaźnik na tablicę napisów - kolejne segmenty wczytanej linii
 */
char** readLine(Map* map, int* segmentCount) {

    *segmentCount = 1;

    int maxSegments = 1;
    int currLength = 0;
    int buffLength = 1;

    char** segments = malloc(sizeof(char*) * (*segmentCount));
    if(segments == NULL) mallocErrorExit(map);

    segments[0] = malloc(sizeof(char) * buffLength);

    if(segments[0] == NULL)
    {
        free(segments);
        mallocErrorExit(map);
    }

    segments[0][0] = '\0';
    bool bad = false;
    bool malloc_err = false;
    bool end = false;

    while(true)
    {
        int c = getchar();

        if(c == '\n') return segments;
        if(c == EOF)
        {
            end = true;
            bad = (*segmentCount > 1 || currLength > 0);
            break;
        }

        if(c == ';')
        {
            if(*segmentCount >= maxSegments)
            {
                char** newSegments = malloc(sizeof(char*) * 2 * (*segmentCount));
                if(newSegments == NULL)
                {
                    malloc_err = true;
                    break;
                }

                for(int i = 0; i < *segmentCount; i++) newSegments[i] = segments[i];
                free(segments);
                segments = newSegments;
                maxSegments *= 2;
            }

            currLength = 0;
            buffLength = 1;
            segments[*segmentCount] = malloc(sizeof(char) * buffLength);
            if(segments[*segmentCount] == NULL)
            {
                malloc_err = true;
                break;
            }

            segments[*segmentCount][0] = '\0';

            (*segmentCount)++;
            continue;
        }

        int segNo = *segmentCount - 1;

        if(currLength >= buffLength - 1)
        {
            char* newBuffer = malloc(sizeof(char) * 2 * buffLength);
            if(newBuffer == NULL)
            {
                malloc_err = true;
                break;
            }

            for(int i = 0; i < currLength; i++) newBuffer[i] = segments[segNo][i];
            free(segments[segNo]);
            segments[segNo] = newBuffer;
            buffLength *= 2;
        }
        segments[segNo][currLength] = c;
        segments[segNo][currLength + 1] = '\0';
        currLength++;
    }

    if(bad || malloc_err)
    {
        for(int i = 0; i < *segmentCount; i++) free(segments[i]);
        free(segments);
    }

    if(malloc_err) mallocErrorExit(map);
    if(end && !bad)
    {
        free(segments[0]);
        free(segments);
        *segmentCount = 0;
        return NULL;
    }
    if(bad) return NULL;

    return segments;
}

/** @brief Konwertuje napis na int64_t
 * @param[in] buff      – wskaźnik na napis
 * @return Liczba zapisana w napisie lub INT64_MIN w przypadku błędu
 */
int64_t toInt64(char* buff)
{
    int64_t a = 0;
    int64_t sgn = 1;
    int i = 0;

    if(buff[0] == '-')
    {
        i++;
        sgn = -1;
    }

    if(buff[i] == '\0') return INT64_MIN;

    while('0' <= buff[i] && buff[i] <= '9')
    {
        if(a > INT64_MAX - (buff[i] - '0') / 10) return INT64_MIN;
        a = a * 10 + buff[i] - '0';
        i++;
    }

    if(buff[i] != '\0') return INT64_MIN;


    return a * sgn;
}

/** @brief Konwertuje napis na rok
 * @param[in] buff      – wskaźnik na napis
 * @return Rok lub 0 w przypadku napisu niebędącego poprawnym rokiem
 */
int toYear(char* buff)
{
    int64_t a = toInt64(buff);
    if(a > INT_MAX || a < INT_MIN) return 0;
    return a;
}

/** @brief Konwertuje napis na długość drogi
 * @param[in] buff      – wskaźnik na napis
 * @return Długość drogi lub 0 w przypadku napisu niebędącego poprawną długością drogi
 */
unsigned toLength(char* buff)
{
    int64_t a = toInt64(buff);
    if(a < 0 || a > UINT_MAX) return 0;
    return a;
}

/** @brief Konwertuje napis na uint,
 * @param[in] buff      – wskaźnik na napis
 * @param[out] ok       – czy napis byl poprawnym uintem
 * @return Długość drogi lub 0 w przypadku napisu niebędącego poprawnym uintem
 */
unsigned toUnsigned(char* buff, bool* ok)
{
    *ok = true;
    int64_t a = toInt64(buff);
    if(a < 0 || a > UINT_MAX)
    {
        *ok = false;
        return 0;
    }

    return a;
}

/** @brief Konwertuje napis na ID drogi krajowej
 * @param[in] buff      – wskaźnik na napis
 * @return ID drogi krajowej lub 0 w przypadku napisu niebędącego poprawnym ID drogi krajowej
 */
unsigned toRouteId(char* buff)
{
    unsigned a = toLength(buff);
    if(a > 999) return 0;
    return a;
}

/** @brief Sprawdza czy napis jest poprawną nazwą miasta
 * @param[in] buff      – wskaźnik na napis
 * @return Wartość true jeśli jest lub false jeśli nie jest
 */
bool validCityName(char* buff)
{
    return getHash(buff) != INVALID_DATA;
}

void SignalError()
{
    fprintf(stderr, "ERROR %d\n", lineNo);
}

/** @brief Zwalnia tablicę segmentów
 * @param[in,out] segments      – wskaźnik na tablicę napisów
 * @param[in] count             – liczba napisów w tablicy
 */
void freeSegments(char** segments, int count)
{
    for(int i = 0; i < count; i++)
    {
        free(segments[i]);
    }
    free(segments);
}

/** @brief Tworzy InputData dla ignorowanej linii i sygnalizuje błąd
 * @return Wskaźnik na InputData lub NULL gdy nie udało się zaalokować pamięci
 */
InputData* wrongLine()
{
    SignalError();
    return newInputData(IGNORE_LINE, 0);
}

/** @brief Tworzy InputData dla linii kończącej
 * @return Wskaźnik na InputData lub NULL gdy nie udało się zaalokować pamięci
 */
InputData* endLine()
{
    return newInputData(END_LINE, 0);
}

/** @brief Tworzy InputData dla ignorowanej linii
 * @return Wskaźnik na InputData lub NULL gdy nie udało się zaalokować pamięci
 */
InputData* commentLine()
{
    return newInputData(IGNORE_LINE, 0);
}

InputData* parseLine(Map* map)
{
    lineNo++;
    int len = 0;
    char** segments = readLine(map, &len);

    if(segments == NULL)
    {
        if(len == 0) return endLine();
        return wrongLine();
    }

    if(len == 0)
    {
        freeSegments(segments, len);
        return endLine();
    }

    if(len == 1 && segments[0][0] == '\0')
    {
        freeSegments(segments, len);
        return commentLine();
    }

    if(segments[0][0] == '#')
    {
        freeSegments(segments, len);
        return commentLine();
    }

    char* first = segments[0];

    if(toRouteId(first) != 0)
    {
        if(len % 3 != 2 || len < 5)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        InputData* data = newInputData(ADD_ROUTE_LINE, len / 3 + 1);
        if(data == NULL)
        {
            freeSegments(segments, len);
            mallocErrorExit(map);
        }

        data->routeId = toRouteId(first);

        data->cities[len / 3] = copyName(segments[len - 1]);
        if(!validCityName(data->cities[len / 3]))
        {
            freeSegments(segments, len);
            freeInputData(data);
            return wrongLine();
        }

        for(int i = 0; i < len / 3; i++)
        {

            data->cities[i] = copyName(segments[i * 3 + 1]);
            data->lengths[i] = toLength(segments[i * 3 + 2]);
            data->years[i] = toYear(segments[i * 3 + 3]);

            if(data->cities[i] == NULL)
            {
                freeSegments(segments, len);
                freeInputData(data);
                mallocErrorExit(map);
            }

            if(data->lengths[i] == 0 || data->years[i] == 0 || !validCityName(data->cities[i]))
            {
                freeSegments(segments, len);
                freeInputData(data);
                return wrongLine();
            }
        }

        freeSegments(segments,len);

        return data;
    }
    else if(strcmp(first, "addRoad") == 0)
    {
        if(len != 5)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        unsigned length = toLength(segments[3]);
        int year = toYear(segments[4]);

        if(!validCityName(segments[1]) || !validCityName(segments[2]) || length == 0 || year == 0)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        InputData* data = newInputData(ADD_ROAD_LINE, 2);
        if(data == NULL)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        data->cities[0] = segments[1];
        data->cities[1] = segments[2];
        data->lengths[0] = length;
        data->years[0] = year;

        free(segments[0]);
        free(segments[3]);
        free(segments[4]);
        free(segments);

        return data;

    }
    else if(strcmp(first, "repairRoad") == 0)
    {
        if(len != 4)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        int year = toYear(segments[3]);

        if(!validCityName(segments[1]) || !validCityName(segments[2]) || year == 0)
        {
            freeSegments(segments, len);
            return wrongLine();
        }


        InputData* data = newInputData(REPAIR_ROAD_LINE, 2);
        if(data == NULL)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        data->cities[0] = segments[1];
        data->cities[1] = segments[2];
        data->years[0] = year;

        free(segments[0]);
        free(segments[3]);
        free(segments);

        return data;

    }
    else if(strcmp(first, "getRouteDescription") == 0)
    {

        if(len != 2)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        bool proper = true;

        unsigned routeId = toUnsigned(segments[1], &proper);

        if(!proper)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        InputData* data = newInputData(ROUTE_DESCRIPTION_LINE, 1);
        if(data == NULL)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        data->routeId = routeId;

        free(segments[0]);
        free(segments[1]);
        free(segments);

        return data;

    }
    else if(strcmp(first, "newRoute") == 0)
    {
        if(len != 4)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        unsigned routeId = toRouteId(segments[1]);

        if(!validCityName(segments[2]) || !validCityName(segments[3]) || routeId == 0)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        InputData* data = newInputData(NEW_ROUTE_LINE, 2);
        if(data == NULL)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        data->routeId = routeId;
        data->cities[0] = segments[2];
        data->cities[1] = segments[3];

        free(segments[0]);
        free(segments[1]);
        free(segments);

        return data;
    }
    else if(strcmp(first, "extendRoute") == 0)
    {
        if(len != 3)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        unsigned routeId = toRouteId(segments[1]);

        if(!validCityName(segments[0]) || routeId == 0)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        InputData* data = newInputData(EXTEND_ROUTE_LINE, 1);
        if(data == NULL)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        data->routeId = routeId;
        data->cities[0] = segments[2];

        free(segments[0]);
        free(segments[1]);
        free(segments);

        return data;
    }
    else if(strcmp(first, "removeRoad") == 0)
    {
         //printf("LMAO\n");
        if(len != 3)
        {
            freeSegments(segments, len);
            return wrongLine();
        }
        //printf("XD\n");
        //for(int i = 0; i < len; i++) printf("%s\n",segments[i]);
        if(!validCityName(segments[1]) || !validCityName(segments[2]))
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        InputData* data = newInputData(REMOVE_ROAD_LINE, 2);
        if(data == NULL)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        data->cities[0] = segments[1];
        data->cities[1] = segments[2];

        free(segments[0]);
        free(segments);

        return data;
    }
    else if(strcmp(first, "removeRoute") == 0)
    {
        if(len != 2)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        unsigned routeId = toRouteId(segments[1]);

        InputData* data = newInputData(REMOVE_ROUTE_LINE, 1);
        if(data == NULL)
        {
            freeSegments(segments, len);
            return wrongLine();
        }

        data->routeId = routeId;

        freeSegments(segments, len);

        return data;
    }

    freeSegments(segments, len);
    return wrongLine();
}
