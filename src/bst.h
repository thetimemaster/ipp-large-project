/** @file
 * @brief Interfejs zrównoważonego drzewa binarnego.
 * Implementacja drzewa binarneogo TREAP równoważonego lowowymi wagami
 * ustawionymi w kopiec. Używa kluczay typu int64_t przechowujce
 * wskaźniki na dane będące podtypami BSTData.
 *
 * @author Piotr Kowalewski <pk406605@students.mimuw.edu.pl>
 */

#ifndef __BST_H__
#define __BST_H__

#include <stdlib.h>
#include <inttypes.h>
#include <stdbool.h>

#define NO_DATA NULL ///< jasna sygnalizacja braku danych w dodawanym węźle

#define CITY_DATA  1 ///< sygnalizacja typu CityData dla polecenia freeBST
#define ROAD_DATA  2 ///< sygnalizacja typu RoadData dla polecenia freeBST
#define DX_DATA    3 ///< sygnalizacja typu DxData dla polecenia freeBST
#define ROUTE_DATA 4 ///< sygnalizacja typu RouteData dla polecenia freeBST
#define DONT_FREE  5 ///< sygnalizacja braku danych w drzewie dla polecenia freeBST
#define PQ_DATA    6 ///< sygnalizacja typu PqData dla polecenia freeBST

typedef struct BSTNode BSTNode;
typedef union BSTData BSTData;

/**
 * Podtyp BSTData, przechowuje dane o Mieście.
 */
typedef struct CityData
{
	const char* name; ///< wskażnik na napis nazwę miasta
	BSTNode* roads;   ///< wskażnik na BST trzymające dla hasza miasta połączonego z tym wskaźnik na RoadData - dane o łączącej je drodze
	BSTNode* routes;  ///< wskaźnik na BST trzymające dla id drogi krajowej wskaźnik na CityData kolejnego miasta na tej drodze krajowej (lub NULL dla ostatniego miasta)
	int64_t hash;     ///< hasz nazwy miasta
} CityData;

/**
 * Podtyp BSTData, przechowuje dane o Drodze.
 */
typedef struct RoadData
{
	BSTNode* routes; ///< wsaźnik na BST o kluczach - routeId dróg krajowych zawierających te droge, wszystkie BSTData w nim to NULL.
	int refs;        ///< Ile miast zawiera jeszcze wskaźnik na ten obiekt, do czyszczenia pamięci
	int year;        ///< rok budowy/ostatniego remontu drogi
	unsigned length; ///< długość drogi
} RoadData;

/**
 * Podtyp BSTData, przechowuje dodatkowe dane o mieście potrzebne dla Dixtry.
 */
typedef struct DXData
{
	int64_t distance; ///< odległość od miasta - początku Dixtry
	BSTNode* from;    ///< wskaźnik na węzeł w głównym BST miast w mapie, bezpośredni poprzednik na trasie do tego miasta, NULL jeśli trasa niejednoznaczna
	int year;         ///< najwcześniejszy rok budowy/ostatniego remontu na trasie do miasta
	int year2;        ///< najwcześniejszy rok budowy/ostatniego remontu na drugiej najlepszej trasie do miasta
} DXData;

/**
 * Podtyp BSTData, używany w kolejce priorytetowej:
 * - I poziom kolejki indeksowany odległością: trzyma wskaźnik na BST - II poziom kolejki dla danej odległości
 * - II poziom kolejki indeksowany rokiem: trzyma wskaźnik na BST - III poziom kolejki
 * - III poziom kolejki indeksowany haszem miasta: trzyma wskaźnik na węzeł danego miasta w głównym drzewie BST
 */
typedef struct PQData
{
	BSTNode* childTree; ///< wskaźnik na kolejne drzewo
} PQData;

/**
 * Podtyp BSTData, przechowuje dane o Drodze Krajowej.
 */
typedef struct RouteData
{
	unsigned id;    ///< id drogi krajowej
	BSTNode* start; ///< wskaźnik na węzeł głównego BST miast - początek drogi krajowej
	BSTNode* end;   ///< wskaźnik na węzeł głównego BST miast - koniec drogi krajowej
} RouteData;

/**
 * Union BSTData przechowuje pewne w drzewie BST dane zależnie od podtypu.
 */
typedef union BSTData
{
	CityData city;   ///< dane o mieście
	RoadData road;   ///< dane o drodze
	DXData dx;       ///< dodatkowe dane o mieście dla Dixtry
	PQData pq;       ///< używany w kolejc priorytetowej
	RouteData route; ///< dane o drodze krajowej
} BSTData;

/**
 * Węzeł w drzewie BST.
 */
typedef struct BSTNode
{
	int treapWeight; ///< waga wierzchołka w stosie
	int64_t key;     ///< klucz wierzchołka
	BSTNode* lson;   ///< wskaźnik na lewego syna w BST
	BSTNode* rson;   ///< wskaźnik na prawego syna w BST
	BSTData* data;   ///< wskaźnik na przechowywane dla klucza dane
} BSTNode;



/** @brief Konstruktor CityData
 * Konstruuje BSTData z ustalonymi wartościami dla podtypu CityData
 * @return Wskaźnik na nowy obiekt BSTData lub NULL jeśli nie udało się zaalokować pamięci.
 */
BSTData* newCityData(char* name, int64_t hash);

/** @brief Konstruktor RoadData
 * Konstruuje BSTData z ustalonymi wartościami dla podtypu RoadData
 * @return Wskaźnik na nowy obiekt BSTData lub NULL jeśli nie udało się zaalokować pamięci.
 */
BSTData* newRoadData(unsigned length, int year);

/** @brief Konstruktor RouteData
 * Konstruuje BSTData z ustalonymi wartościami dla podtypu RouteData
 * @return Wskaźnik na nowy obiekt BSTData lub NULL jeśli nie udało się zaalokować pamięci.
 */
BSTData* newRouteData(unsigned id, BSTNode* begin, BSTNode* end);

/** @brief Konstruktor DxData
 * Konstruuje BSTData z ustalonymi wartościami dla podtypu DxData
 * @return Wskaźnik na nowy obiekt BSTData lub NULL jeśli nie udało się zaalokować pamięci
 */
BSTData* newDxData(int year, int year2, int64_t distance, BSTNode* from);

/** @brief Konstruktor PqData
 * Konstruuje BSTData z ustalonymi wartościami dla podtypu PqData
 * @return Wskaźnik na nowy obiekt BSTData lub NULL jeśli nie udało się zaalokować pamięci
 */
BSTData* newPqData(BSTNode* childTree);


/** @brief Wyszukuje węzeł odpowiadający kluczowi w BST
 * @param[in] key    – szukany klucz
 * @param[in] in     – wskaźnik na BST w którym szukamy klucza
 * @return Wskaźnik na znaleziony węzeł lub NULL jeśli klucz nie występuje w BST.
 */
BSTNode* bstFind(int64_t key, BSTNode* in);


/** @brief Wyszukuje węzeł o minimalnym kluczu w BST
 * @param[in] in     – wskaźnik na BST w którym szukamy klucza
 * @return Wskaźnik na znaleziony węzeł lub NULL jeśli klucz nie występuje w BST.
 */
BSTNode* bstMin(BSTNode* in);

/** @brief Wyszukuje węzeł o maksymalnym kluczu w BST
 * @param[in] in     – wskaźnik na BST w którym szukamy klucza
 * @return Wskaźnik na znaleziony węzeł lub NULL jeśli klucz nie występuje w BST.
 */
BSTNode* bstMax(BSTNode* in);

/** @brief Dodaje nowy węzeł do BST
 * Węzeł o podanym kluczu nie może istnieć w danym BST, jest to sprawdzane assertem
 * @param[in] key        – klucz nowego węzła
 * @param[in] data       – wskaźnik na BSTData przechowywane pod tym kluczem
 * @param[in] in         – wskaźnik na korzeń BST w którym dodajemy
 * @param[in,out] flags  – flagi do polecenia
 * @return Nowy wkaźnik na korzeń tego BST, zmiany we flagach:
 * - MALLOC_ERROR włączone jeśli nie udało się zaalokować pamięci
 */
BSTNode* bstInsert(int64_t key, BSTData* data, BSTNode* in, int* flags);


/** @brief Usuwa węzeł z BST
 * Jeśli węzeł o danym kluczu nie istnieje to nic nie robi.
 * @param[in] key        – klucz do usunięcia
 * @param[in] in         – wskaźnik na korzeń BST w którym usuwamy
 * @param[in] freeData   – czy powinien zwolnić przechowywany BSTData
 * @return Nowy wkaźnik na korzeń tego BST
 */
BSTNode* bstRemove(int64_t key, BSTNode* in, bool freeData);

/** @brief Przekształca BST w tymczasową tablicę
 * @param[in] node     – wskaźnik na BST
 * @param[out] outSize – liczba komórek w nowej tablicy
 * @return Tablica wskażników na kolejne węzły w BST posortowane po kluczach
 * zakończona NULLem lub NULL w przypadku gdy nie udało się zaalokować pamięci.
 */
BSTNode** bstToArray(BSTNode* node, int* outSize);

/** @brief Zwalnia BST z pamięci
 * Dane w BSTData są zwalniane w zależności od typu drzewa które zwalniamy
 * @param[in] node      – wskaźnik na BST
 * @param[out] dataType – typ danych przechowywanych w BST
 */
void freeBST(BSTNode* node, int dataType);

#endif
