#include "map.h"
#include "bst.h"
#include <limits.h>
#include <stdio.h>
#include <assert.h>

void addRoadToRoute(BSTNode* city1, BSTNode* city2, unsigned routeId, int* flags)
{
	BSTNode* road = bstFind(city2->data->city.hash, city1->data->city.roads);
	road->data->road.routes = bstInsert(routeId, NO_DATA, road->data->road.routes, flags);
}


void removeRoadFromRoute(BSTNode* city1, BSTNode* city2, unsigned routeId)
{
	BSTNode* road = bstFind(city2->data->city.hash, city1->data->city.roads);
	road->data->road.routes = bstRemove(routeId, road->data->road.routes, false);
}

void reverseAddSubroute(BSTNode** nodes, unsigned routeId, int* flags)
{
	int i = 0;
	while(nodes[i + 1] != NULL) i++;

	if(*flags & ROUTE_END)
	{
		nodes[i]->data->city.routes = bstRemove(routeId, nodes[i]->data->city.routes, false);
	}

	i--;

	while(i >= 0)
	{
		nodes[i]->data->city.routes = bstRemove(routeId, nodes[i]->data->city.routes, false);
		removeRoadFromRoute(nodes[i], nodes[i + 1], routeId);
		i--;
	}

	if(*flags & APPEND_ROUTE)
		nodes[0]->data->city.routes = bstInsert(routeId, NO_DATA, nodes[0]->data->city.routes, flags);
}

void addSubroute(BSTNode** nodes, unsigned routeId, int* flags)
{
	if(*flags & APPEND_ROUTE)
		nodes[0]->data->city.routes = bstRemove(routeId, nodes[0]->data->city.routes, false);

	int i = 0;
	while(nodes[i + 1] != NULL)
	{
		nodes[i]->data->city.routes =
			bstInsert(routeId, nodes[i + 1]->data, nodes[i]->data->city.routes, flags);

		addRoadToRoute(nodes[i], nodes[i + 1], routeId, flags);
		if(*flags & MALLOC_ERROR)
		{
			reverseAddSubroute(nodes, routeId, flags);
			return;
		}
		i++;
	}

	if(*flags & ROUTE_END)
	{
		nodes[i]->data->city.routes = bstInsert(routeId, NO_DATA, nodes[i]->data->city.routes, flags);
		if(*flags & MALLOC_ERROR)
		{
			reverseAddSubroute(nodes, i - 1, flags);
		}
	}
}

static BSTNode* pqPop(BSTNode* pqBST, BSTNode** outNext, int64_t* length, int* year)
{
	BSTNode* constDistBST = bstMin(pqBST);
	*length = constDistBST->key;

	BSTNode* constYearBST = bstMax(constDistBST->data->pq.childTree);
	*year = constYearBST->key;

	BSTNode* target = bstMin(constYearBST->data->pq.childTree);
	*outNext = target->data->pq.childTree;


	constYearBST->data->pq.childTree = bstRemove((*outNext)->key, constYearBST->data->pq.childTree, true);
	if(constYearBST->data->pq.childTree == NULL)//ostatnie miasto o takim roku i takiej odleglosci
	{
		constDistBST->data->pq.childTree = bstRemove((int64_t)(*year), constDistBST->data->pq.childTree, true);
		if(constDistBST->data->pq.childTree == NULL)//ostatnie miasto otakiej odleglosci
		{
			pqBST = bstRemove(*length, pqBST, true);
		}
	}

	return pqBST;
}

static void freePQ(BSTNode* pqBST)
{
	while(pqBST != NULL)
	{
		BSTNode* node;
		int64_t length;
		int year;

		pqPop(pqBST, &node, &length, &year);
	}
}

static BSTNode* pqAdd(BSTNode* pqBST, BSTNode* node, int64_t length, int year, int* flags)
{
	*flags = 0;

	BSTNode* constDistBST = bstFind(length, pqBST);
	if(constDistBST == NULL)
	{
		BSTData* pqData = newPqData(NULL);
		if(pqData == NULL)
		{
			*flags |= MALLOC_ERROR;
			return NULL;
		}

		pqBST = bstInsert(length, pqData, pqBST, flags);
		if(*flags & MALLOC_ERROR)
		{
			free(pqData);
			return false;
		}

		constDistBST = bstFind(length, pqBST);
	}

	BSTNode* constYearBST = bstFind(year, constDistBST->data->pq.childTree);
	if(constYearBST == NULL)
	{
		BSTData* pqData = newPqData(NULL);
		if(pqData == NULL)
		{
			*flags |= MALLOC_ERROR;
			return NULL;
		}

		constDistBST->data->pq.childTree =
			bstInsert(year, pqData, constDistBST->data->pq.childTree, flags);

		if((*flags) & MALLOC_ERROR)
		{
			free(pqData);
			return false;
		}

		constYearBST = bstFind(year, constDistBST->data->pq.childTree);
	}

	BSTData* pqData = newPqData(node);
	if(pqData == NULL)
	{
		*flags |= MALLOC_ERROR;
		return NULL;
	}

	constYearBST->data->pq.childTree =
		bstInsert(node->data->city.hash, pqData, constYearBST->data->pq.childTree, flags);

	if((*flags) & MALLOC_ERROR)
	{
		free(pqData);
		return false;
	}

	return pqBST;
}

BSTNode** dxtra(Map* map, BSTNode* from, BSTNode* to, unsigned routeId, int* flags, int64_t* outLength, int* outYear)
{
	*outLength = 0;
	*outYear = 0;

	BSTNode* pathBST = bstInsert(from->data->city.hash, newDxData(0, 0, 0, from), NULL, flags);
	if(*flags & MALLOC_ERROR) return NULL;

	BSTNode* pqBST = NULL;
	pqBST = pqAdd(pqBST, from, 0, 0, flags);
	if(*flags & MALLOC_ERROR)
	{
		freeBST(pathBST, DX_DATA);
		return NULL;
	}

	while(pqBST != NULL)
	{
		BSTNode* next;
		int year, year2;
		int64_t length;

		pqBST = pqPop(pqBST, &next, &length, &year);
		BSTNode* currData = bstFind(next->data->city.hash, pathBST);

		//better road to city was used
		assert(currData->data->dx.distance <= length);
		if(currData->data->dx.distance == length) assert(currData->data->dx.year >=year);

		if(currData->data->dx.distance != length || currData->data->dx.year != year) continue;

		year2 = currData->data->dx.year2;

		//unclear route, must have second route with same year
		if(currData->data->dx.from == NULL)
		{
			assert(year2 == year);
		}

		int roadCount;
		BSTNode** roadsArray = bstToArray(next->data->city.roads, &roadCount);
		if(roadsArray == NULL)
		{
			freeBST(pathBST, DX_DATA);
			freePQ(pqBST);
			return NULL;
		}

		int i = 0;
		while(roadsArray[i] != NULL)
		{
			//we cant use roads from route routeId
			if(bstFind(routeId, roadsArray[i]->data->road.routes) != NULL)
			{
				i++;
				continue;
			}

			BSTNode* neighbour = bstFind(roadsArray[i]->key, pathBST);
			if(neighbour == NULL)
			{
				BSTData* data = newDxData(0, 0, LONG_MAX, NULL);
				if(data == NULL)
				{
					free(roadsArray);
					freeBST(pathBST, DX_DATA);
					freePQ(pqBST);
					return NULL;
				}

				pathBST = bstInsert(roadsArray[i]->key, data, pathBST, flags);
				if(*flags & MALLOC_ERROR)
				{
					free(roadsArray);
					freeBST(pathBST, DX_DATA);
					freePQ(pqBST);
					return NULL;
				}

				neighbour = bstFind(roadsArray[i]->key, pathBST);
			}

			BSTNode* neighbourNode = bstFind(roadsArray[i]->key, map->cityTreeRoot);

			//city we are looking for is the only one on route routeID where we can go in
			if(roadsArray[i]->key != to->data->city.hash)
			{
				if(bstFind(routeId, neighbourNode->data->city.routes) != NULL)
				{
					i++;
					continue;
				}
			}

			bool better = false;
			int64_t newLength = length + roadsArray[i]->data->road.length;
			int newYear  = roadsArray[i]->data->road.year;
			int newYear2 = roadsArray[i]->data->road.year;

			if(year2 != 0)
			{
				if(year2 < newYear2) newYear2 = year2;
			}
			else newYear2 = 0;

			if(year != 0) //not the starting point
			{
				if(year < newYear) newYear = year;
			}

			if(neighbour->data->dx.distance == newLength)
			{
				if(neighbour->data->dx.year == newYear)
				{
					neighbour->data->dx.year2 = newYear;
					neighbour->data->dx.from = NULL; //path not clear
				}
				else if(neighbour->data->dx.year < newYear) better = true;
				else if(neighbour->data->dx.year2 < newYear || neighbour->data->dx.year2 == 0)
				{
					neighbour->data->dx.year2 = newYear;
				}
			}
			else if(neighbour->data->dx.distance > newLength) better = true;

			if(better)
			{
				if(neighbour->data->dx.distance == newLength)
				{
					neighbour->data->dx.year2 = neighbour->data->dx.year;
					if(neighbour->data->dx.year2 < newYear2)
						neighbour->data->dx.year2 = newYear2;
				}
				else
				{
					neighbour->data->dx.year2 = newYear2;
				}

				neighbour->data->dx.distance = newLength;
				neighbour->data->dx.year = newYear;

				if(neighbour->data->dx.year2 != neighbour->data->dx.year)
					neighbour->data->dx.from = next;
				else neighbour->data->dx.from = NULL;

				pqBST = pqAdd(pqBST, neighbourNode , newLength, newYear, flags);
				if(*flags & MALLOC_ERROR)
				{
					free(roadsArray);
					freeBST(pathBST, DX_DATA);
					freePQ(pqBST);
					return NULL;
				}
			}
			i++;
		}
		assert(i == roadCount - 1);

		free(roadsArray);
	}

	BSTNode* result = bstFind(to->data->city.hash, pathBST);
	if(result == NULL)
	{
		(*flags) |= NO_PATH;
		freeBST(pathBST, DX_DATA);
		return NULL;
	}

	*outLength = result->data->dx.distance;
	*outYear = result->data->dx.year;

	if(result->data->dx.from == NULL)
	{
		(*flags) |= PATH_UNCLEAR;
		freeBST(pathBST, DX_DATA);
		return NULL;
	}

	int outputSize = 1;
	BSTNode** output = malloc(sizeof(BSTNode*) * outputSize);
	int i = 0;

	BSTNode* current = to;
	while(true)
	{
		if(i + 1 == outputSize)
		{
			BSTNode** newOutput = malloc(sizeof(BSTNode*) * 2 * outputSize);
			if(newOutput == NULL)
			{
				free(output);
				freeBST(pathBST, DX_DATA);
				return NULL;
			}

			for(int j = 0; j <= i; j++) newOutput[j] = output[j];
			free(output);
			output = newOutput;
			outputSize *= 2;
		}

		output[i] = current;
		i++;
		BSTNode* about = bstFind(current->data->city.hash, pathBST);
		assert(about->data->dx.year2 == 0 || about->data->dx.year2 < *outYear);

		if(about->data->dx.from == current) break;

		current = about->data->dx.from;
	}
	output[i] = NULL;

	for(int j = 0; j < i/2; j++)
	{
		BSTNode* swp = output[j];
		output[j] = output[i - 1 - j];
		output[i - 1 - j] = swp;
	}
	freeBST(pathBST, DX_DATA);

	return output;
}
