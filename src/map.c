/** @file
 * Implementacja zadanego interfejsu map.h
 *
 * @author Piotr Kowalewski <pk406605@students.mimuw.edu.pl>
 */

#include <stdbool.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "bst.h"
#include "graph.h"
#include "text.h"

Map* newMap()
{
	Map* map = malloc(sizeof(Map));
	if(map == NULL) return NULL;
	map->cityTreeRoot = NULL;
	map->routes = NULL;
	return map;
}

void deleteMap(Map* map)
{
	if(map == NULL) return;
	freeBST(map->cityTreeRoot, CITY_DATA);
	freeBST(map->routes, ROUTE_DATA);
	free(map);
}

/** @brief Wyszukuje miasto na mapie
 * @param[in] map  – wskaźnik na strukturę Map
 * @param[in] hash – hasz nazwy miasta
 * @return Wskaźnik na węzeł odpowiadający temu miastu w głównym BST miast mapy
 * lub NULL jeśli takie miasto nie istnieje.
 */
static BSTNode* findCity(Map* map, int64_t hash)
{
	BSTNode* node = bstFind(hash, map->cityTreeRoot);
	return node;
}

/** @brief Dodaje nowe miasto do mapy
 * @param[in,out] map – wskaźnik na strukturę Map
 * @param[in] hash    – obliczony hasz nazwy miasta
 * @param[in] name    – wskaźnik na napis – nazwę miasta
 * @return Wskaźnik na węzeł odpowiadający temu miastu w głównym BST miast mapy
 * lub NULL jeśli nie udało się zaalokować pamięci
 */
static BSTNode* addCity(Map* map, int64_t hash, const char* name)
{
	int flags = 0;

	char* nameCopy = copyName((char*)name);
	if(nameCopy == NULL) return NULL;

	BSTData* cityData = newCityData(nameCopy, hash);
	if(cityData == NULL)
	{
		free(nameCopy);
		return NULL;
	}

	map->cityTreeRoot = bstInsert(hash, cityData, map->cityTreeRoot, &flags);
	if(flags & MALLOC_ERROR) return NULL;
	return findCity(map, hash);
}


bool addRoad(Map *map, const char *city1, const char *city2, unsigned length, int builtYear)
{
    //printf("XD %u %d\n",length, builtYear);
	if(map == NULL) return false;

	int64_t hash1 = getHash(city1);
	int64_t hash2 = getHash(city2);

	if(hash1 == INVALID_DATA || hash2 == INVALID_DATA || hash1 == hash2) return false;
	if(builtYear == 0 || length == 0) return false; //invalid data

	BSTNode* node1 = findCity(map, hash1);
	if(node1 == NULL)
	{
		node1 = addCity(map, hash1, city1);
		if(node1 == NULL) return false; //malloc_error
	}

	BSTNode* node2 = findCity(map, hash2);
	if(node2 == NULL)
	{
		node2 = addCity(map, hash2, city2);
		if(node2 == NULL) return false; //malloc_error
	}

	if(bstFind(hash2, node1->data->city.roads) == NULL)
	{
		int a = 0;
		BSTData* roadData = newRoadData(length, builtYear);
		if(roadData == NULL) return false; //malloc_error

		node1->data->city.roads = bstInsert(hash2, roadData, node1->data->city.roads, &a);
		if(a & MALLOC_ERROR) return false; //malloc_error

		node2->data->city.roads = bstInsert(hash1, roadData, node2->data->city.roads, &a);
		if(a & MALLOC_ERROR)
		{
			node1->data->city.roads = bstRemove(hash2, node1->data->city.roads, false);
			return false; //malloc_error
		}

		return true;
	}
	else return false; //already connected
}

bool repairRoad(Map *map, const char *city1, const char *city2, int repairYear)
{
	if(map == NULL) return false;

	int64_t hash1 = getHash(city1);
	int64_t hash2 = getHash(city2);

	if(hash1 == INVALID_DATA || hash2 == INVALID_DATA || hash1 == hash2) return false;
	if(repairYear == 0) return false; //invalid data

	BSTNode* node1 = bstFind(hash1, map->cityTreeRoot);
	BSTNode* node2 = bstFind(hash2, map->cityTreeRoot);
	if(node1 == NULL || node2 == NULL) return false; //nonexistant city

	BSTNode* road = bstFind(hash2, node1->data->city.roads);
	if(road == NULL) return false; //road does not exist

	if(repairYear < road->data->road.year) return false;
	road->data->road.year = repairYear;

	return true;
}

bool newRoute(Map* map, unsigned routeId, const char *city1, const char *city2)
{
	if(map == NULL) return false;

	int64_t hash1 = getHash(city1);
	int64_t hash2 = getHash(city2);

	if(routeId == 0 || routeId > 999) return false;
	if(hash1 == INVALID_DATA || hash2 == INVALID_DATA || hash1 == hash2) return false; //invalid data

	BSTNode* node1 = bstFind(hash1, map->cityTreeRoot);
	BSTNode* node2 = bstFind(hash2, map->cityTreeRoot);
	if(node1 == NULL || node2 == NULL) return false; //nonexistant city

	if(bstFind(routeId, map->routes) != NULL) return false; //already exists

	int flags = 0;
	int worstYear;
	int64_t length;

	BSTNode** nodes = dxtra(map, node1, node2, routeId, &flags, &length, &worstYear);
	if(flags & MALLOC_ERROR || flags & NO_PATH || flags & PATH_UNCLEAR) return false;

	flags |= ROUTE_END;
	addSubroute(nodes, routeId, &flags);
	if(flags & MALLOC_ERROR) return false;

	BSTData* routeData = newRouteData(routeId, node1, node2);
	if(routeData == NULL)
	{
		reverseAddSubroute(nodes, routeId, &flags);
		return false;
	}

	map->routes = bstInsert(routeId, routeData, map->routes, &flags);
	if(flags & MALLOC_ERROR)
	{
		reverseAddSubroute(nodes, routeId, &flags);
		return false;
	}

	free(nodes);

	return true;
}

bool newRouteExact(Map *map, unsigned routeId, char** cities, int* years, unsigned* lengths, int count) {

    if (map == NULL) return false;

    if(bstFind(routeId, map->routes) != NULL) return false;

    if (cities == NULL || years == NULL || lengths == NULL) return false;

    BSTNode* cityNodes[count + 1];

    for (int i = 0; i < count; i++)
    {
        int64_t hash = getHash(cities[i]);
        cityNodes[i] = findCity(map, hash);

        if(cityNodes[i] == NULL)
        {
            cityNodes[i] = addCity(map, hash, cities[i]);
            if(cityNodes[i] == NULL) return false;
        }

        for(int j = 0; j < i; j++)
        {
            if(cityNodes[i] == cityNodes[j]) return false;
        }

        if(i > 0)
        {
            BSTNode* road = bstFind(hash, cityNodes[i - 1]->data->city.roads);

            if(road != NULL)
            {
                if(road->data->road.length != lengths[i - 1]) return false;
                if(road->data->road.year > years[i - 1]) return false;
            }
        }
    }

    for (int i = 0; i < count; i++)
    {
        int64_t hash = getHash(cities[i]);

        if(i > 0)
        {
            BSTNode* road = bstFind(hash, cityNodes[i - 1]->data->city.roads);

            if(road == NULL)
            {
                if(!addRoad(map, cities[i - 1], cities[i], lengths[i - 1], years[i - 1])) return false;
            }
            else
            {
                if(!repairRoad(map, cities[i - 1], cities[i], years[i - 1])) return false;
            }
        }
    }
    cityNodes[count] = NULL;



    int flags = ROUTE_END;
    addSubroute(cityNodes, routeId, &flags);

    if(flags & MALLOC_ERROR) return false;

    BSTData* data = newRouteData(routeId, cityNodes[0], cityNodes[count - 1]);
    if(data == NULL) return false;

    map->routes = bstInsert(routeId, data, map->routes, &flags);

    return true;
}

bool extendRoute(Map* map, unsigned routeId, const char* city)
{
	if(map == NULL) return NULL;

	int64_t hash = getHash(city);

	if(routeId == 0 || routeId > 999) return false;
	if(hash == INVALID_DATA) return false; //invalid data

	BSTNode* node = bstFind(hash, map->cityTreeRoot);
	if(node == NULL) return false; //nonexistant city

	BSTNode* routeData = bstFind(routeId, map->routes);
	if(routeData == NULL) return false; //nonexistant route

	if(bstFind(routeId, node->data->city.routes) != NULL) return false; //on route

	int flags1 = 0, flags2 = 0;
	int worstYear1, worstYear2;
	int64_t length1, length2;

	BSTNode** route1 = dxtra(map, node, routeData->data->route.start,
							routeId, &flags1, &length1, &worstYear1);
	if(flags1 & MALLOC_ERROR) return false;

	BSTNode** route2 = dxtra(map, routeData->data->route.end, node,
							routeId, &flags2, &length2, &worstYear2);
	if(flags2 & MALLOC_ERROR) return false;

	int selected = -1;

	if(flags1 & NO_PATH)
	{
		if((flags2 & NO_PATH) == 0) selected = 2;
	}
	else
	{
		if(flags2 & NO_PATH) selected = 1;
		else
		{
			if(length1 != length2)
			{
				if(length1 < length2) selected = 1;
				else selected = 2;
			}
			else if(worstYear1 != worstYear2)
			{
				if(worstYear1 > worstYear2) selected = 1;
				else selected = 2;
			}
		}
	}

	if(selected == 1 && (flags1 & PATH_UNCLEAR) == 0)
	{
		addSubroute(route1, routeId, &flags1);
		free(route1);
		free(route2);
		if(flags1 & MALLOC_ERROR) return false;
		routeData->data->route.start = node;
		return true;
	}
	if(selected == 2 && (flags2 & PATH_UNCLEAR) == 0)
	{
		flags2 |= APPEND_ROUTE;
		flags2 |= ROUTE_END;
		addSubroute(route2, routeId, &flags2);
		free(route1);
		free(route2);
		if(flags2 & MALLOC_ERROR) return false;
		routeData->data->route.end = node;
		return true;
	}

	free(route1);
	free(route2);
	return false;
}

bool removeRoute(Map *map, unsigned routeId)
{
    if(map == NULL) return false;

    BSTNode* _node = bstFind(routeId, map->routes);
    BSTData* data = (_node == NULL ? NULL : _node->data);

    if(data == NULL) return false;

    BSTData* node = data->route.start->data;

    while(true)
    {
        BSTData* next = bstFind(routeId, node->city.routes)->data;
        node->city.routes = bstRemove(routeId, node->city.routes, false);

        if(next == NULL) break;

        BSTData* road = bstFind(next->city.hash, node->city.roads)->data;
        road->road.routes = bstRemove(routeId, road->road.routes, false);
        node = next;
    }

    map->routes = bstRemove(routeId, map->routes, true);

    return true;
}

bool removeRoad(Map *map, const char* city1, const char* city2)
{
	if(map == NULL) return false;

	int64_t hash1 = getHash(city1);
	int64_t hash2 = getHash(city2);

	if(hash1 == INVALID_DATA || hash2 == INVALID_DATA || hash1 == hash2) return false; //invalid data

	BSTNode* node1 = bstFind(hash1, map->cityTreeRoot);
	BSTNode* node2 = bstFind(hash2, map->cityTreeRoot);
	if(node1 == NULL || node2 == NULL) return false; //nonexistant city

	BSTNode* road = bstFind(hash2, node1->data->city.roads);
	if(road == NULL) return false; //road does not exist

	int routeCount;
	BSTNode** routes = bstToArray(road->data->road.routes, &routeCount);
	if(routes == NULL) return false; //malloc error

	BSTNode*** newRoutes = malloc(sizeof(BSTNode**) * routeCount);
	if(routes == NULL)
	{
		free(routes);
		return false;
	}

	routeCount--; //odliczamy NULLa na końcu tablicy

	bool isOk = true;

	for(int i = 0; i < routeCount; i++)
	{
		BSTNode* next = bstFind(routes[i]->key, node1->data->city.routes);
		int flags = 0, worstYear;
		int64_t length;

		assert(next != NULL);
		if(next->data != node2->data)
		{
			newRoutes[i] = dxtra(map, node2, node1, routes[i]->key, &flags, &length, &worstYear);
			if(flags & MALLOC_ERROR || flags & NO_PATH || flags & PATH_UNCLEAR) isOk = false;
		}
		else
		{
			newRoutes[i] = dxtra(map, node1, node2, routes[i]->key, &flags, &length, &worstYear);
			if(flags & MALLOC_ERROR || flags & NO_PATH || flags & PATH_UNCLEAR) isOk = false;
		}
	}

	if(!isOk)
	{
		for(int i = 0; i < routeCount; i++) free(newRoutes[i]);
		free(newRoutes);
		free(routes);
		return false;
	}

	int flags = 0;
	flags |= APPEND_ROUTE;
	for(int i = 0; i < routeCount; i++)
	{
		addSubroute(newRoutes[i], routes[i]->key, &flags);
		if(flags & MALLOC_ERROR)
		{
			for(int j = 0; j < i; j++)
				reverseAddSubroute(newRoutes[i], routes[i]->key, &flags);

			for(int j = 0; j < routeCount; i++) free(newRoutes[j]);
			free(newRoutes);
			free(routes);
			return false;
		}
	}

	for(int i = 0; i < routeCount; i++) free(newRoutes[i]);
	free(newRoutes);
	free(routes);

	freeBST(road->data->road.routes, DONT_FREE);
	free(road->data);

	node1->data->city.roads = bstRemove(hash2, node1->data->city.roads, false);
	node2->data->city.roads = bstRemove(hash1, node2->data->city.roads, false);

	return true;
}

char const* getRouteDescription(Map *map, unsigned routeId)
{
	if(map == NULL) return emptyString();

	BSTNode* routeData = bstFind(routeId, map->routes);
	if(routeData == NULL) return emptyString();

	int buffLength = 1;
	int length = 0;

	char* desc = malloc(sizeof(char) * buffLength);
	if(desc == NULL) return emptyString();
	desc[0] = '\0';

	desc = appendNumber(desc, routeId, &length, &buffLength);

	BSTNode* current = routeData->data->route.start;
	while(current->data != routeData->data->route.end->data)
	{
		desc = appendText(desc, (char*)current->data->city.name, &length, &buffLength);
		BSTNode* next = bstFind(routeId, current->data->city.routes);
		BSTNode* road = bstFind(next->data->city.hash, current->data->city.roads);
		desc = appendNumber(desc, road->data->road.length, &length, &buffLength);
		desc = appendNumber(desc, road->data->road.year, &length, &buffLength);
		current = next;
	}

	desc = appendText(desc, (char*)current->data->city.name, &length, &buffLength);
	if(desc == NULL) return emptyString();
	desc[length - 1] = '\0';
	return (char const*)desc;
}

void mallocErrorExit(Map* map)
{
    deleteMap(map);
    exit(0);
}
