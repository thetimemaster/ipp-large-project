/** @file
 * Interfejs do obsługi wewnętrznych operacji na grafie dróg w Map
 *
 * @author Piotr Kowalewski <pk406605@students.mimuw.edu.pl>
 */

#ifndef __GRAPH_H__
#define __GRAPH_H__

#include "bst.h"
#include "map.h"

/** @brief Dodaje informacje o przejściu drogi krajowej przez drogę między miastami
 * Jeśli nie uda się zaalokować pamięci to zostanie ustawiona flaga MALLOC_ERROR
 * @param[in] city1       – wskaźnik na węzeł w głównym BST miast pierwszego z miast
 * @param[in] city2       – wskaźnik na węzeł w głównym BST miast drugiego z miast
 * @param[in] routeId     – id usuwanej drogi krajowej
 * @param[in,out] flags   – dodatkowe flagi
 */
void addRoadToRoute(BSTNode* city1, BSTNode* city2, unsigned routeId, int* flags);

/** @brief Usuwa droge krajową z przechodzoących przez daną drogę
 * @param[in] city1   – wskaźnik na węzeł w głównym BST miast pierwszego z miast
 * @param[in] city2   – wskaźnik na węzeł w głównym BST miast drugiego z miast
 * @param[in] routeId – id usuwanej drogi krajowej
 */
void removeRoadFromRoute(BSTNode* city1, BSTNode* city2, unsigned routeId);

/** @brief Cofa poprzednie addSubroute
 * @param[in] nodes     – wskaźnik na tablicę, ciąg wskaźników na kolejne miasta zakończony NULLem
 * @param[in] routeId   – id drogi krajowej do której dodajemy miasta
 * @param[in,out] flags – dodatkowe flagi:
 * - MALLOC_ERROR zostaje ustawiony jeśli nie dało się zaalokować pamięci
 * - APPEND_ROUTE powinno być włączone jeśli początek ciągu miast był już na tej drodze
 * - ROUTE_END powinno być włączone jeśli dodajemy na koniec drogi krajowej
 */
void reverseAddSubroute(BSTNode** nodes, unsigned routeId, int* flags);

/** @brief Dodaje ciąg miast jako nową część drogi krajowej
 * @param[in] nodes     – wskaźnik na tablicę, ciąg wskaźników na kolejne miasta zakończony NULLem
 * @param[in] routeId   – id drogi krajowej do której dodajemy miasta
 * @param[in,out] flags – dodatkowe flagi:
 * - MALLOC_ERROR zostaje ustawiony jeśli nie dało się zaalokować pamięci
 * - APPEND_ROUTE powinno być włączone jeśli początek ciągu miast był już na tej drodze
 * - ROUTE_END powinno być włączone jeśli dodajemy na koniec drogi krajowej
 */
void addSubroute(BSTNode** nodes, unsigned routeId, int* flags);


/** @brief Wyszukuje najoptymalniejszą trasę dla drogi krajowej pomiędzy miastami.
 * Trasa musi być wyznaczona jednoznacznie przy czym:
 * - nie przechodzi przez żadne węzły znajdujące się już na drodze routeId (poza from i to)
 * - w pierwszej kolejności minimalizujemy sumaryczną długość krawędzi
 * - w drugiej kolejności minimalizujemy najwcześniejszy rok budowy/remontu drogi
 * - gdy dalej istnieje wiele możliwych tras uznajemy ze nie ma jednoznacznie najlepszej
 * @param[in,out] map    – wskaźnik na strukturę przechowującą mapę dróg;
 * @param[in] from       – wskaźnik na węzeł w BST odpowiadający miastu z którego szukamy trasy
 * @param[in] to         – wskaźnik na węzeł w BST odpowiadający miastu do którego szukamy trasy
 * @param[in] routeId    – id drogi krajowej dla której szukamy trasy
 * @param[in,out] flags  – flagi do polecenia
 * @param[out] outLength – długość trasy (o ile znaleziono)
 * @param[out] outYear   – najgorszy rok na trasie (o ile znaleziono)
 * @return Wskaźnik na tablicę wskażników na węzły w BST odpowiadające kolejnym miastom
 * na trasie zakończoną NULLEm lub NULL jeśli coś poszło nie tak, dane we flagach:
 * - MALLOC_ERROR we flagach jeśli nie udało się zaalokować pamięci
 * - NO_PATH jeśli nie udało się znaleźć trasy.
 * - PATH_UNCLEAR jeśli trasa nie jest wyznaczona jednoznacznie
 */
BSTNode** dxtra(Map* map, BSTNode* from, BSTNode* to, unsigned routeId, int* flags, int64_t* outLength, int* outYear);

#endif
