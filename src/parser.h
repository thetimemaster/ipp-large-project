/** @file
 * @brief Interfejs do wczytywania danych z wejścia
 * @author Piotr Kowalewski <pk406605@students.mimuw.edu.pl>
 */


#ifndef __PARSER_H__
#define __PARSER_H__

#include "map.h"

#define IGNORE_LINE 1 ///< Typ wczytanej linii: linia błędna/komentarz
#define ADD_ROAD_LINE 2 ///< Typ wczytanej linii: Komenda addRoad
#define REPAIR_ROAD_LINE 3 ///< Typ wczytanej linii: Komenda repairRoad
#define ROUTE_DESCRIPTION_LINE  4 ///< Typ wczytanej linii: getRouteDescription
#define ADD_ROUTE_LINE 5 ///< Typ wczytanej linii: komenda dodająca drogę krajową (exact)
#define END_LINE 6 ///< Typ wczytanej linii: brak (koniec wejścia)
#define NEW_ROUTE_LINE 7 ///< Typ wczytanej linii: Komenda newRoute
#define EXTEND_ROUTE_LINE 8 ///< Typ wczytanej linii: Komenda extendRoute
#define REMOVE_ROAD_LINE 9 ///< Typ wczytanej linii: Komenda removeRoad
#define REMOVE_ROUTE_LINE 10 ///< Typ wczytanej linii: Komenda removeRoute

/**
 * Struktura zawierająca dane z jednej linii wejścia
 */
typedef struct InputData
{
    int lineType; ///< Typ linii, jak w \#defineach
    char** cities; ///< Tablica wskaźników na kolejne nazwy miast na wejściu (jeśli były)
    int* years; ///< Tablica kolejnych lat na wejściu(jeśli były)
    unsigned* lengths; ///< Tablica kolejnych długości dróg na wejściu (jeśli były)
    int routeId; ///< ID drogi krajowej na wejściu (jeśli było)
    int inputSize; ///< Wielkość tablic w strukturze
} InputData;




/** @brief Zygnalizuje błędną linię
 * Wypisuje na standardowe wyjście diagnostyczne linię "ERROR (lineNo)"
 * gdzie lineNo jest aktualnym numerem linii. Wczytywane linie są indeksowane
 * od jedynki.
 */
void SignalError();

/** @brief Zwalnia strukturę InputData
 * @param[in, out] data  – wskaźnik na strukturę
 */
void freeInputData(InputData* data);

/** @brief Wczytuje jedną linię z wejścia
 * Sprawdzana jest formalna poprawność danych wejściowych, w przypadku problemu
 * z alokacją pamięci na wczytanie danych funkcja zwalnia pamięć zajmowaną przez
 * podaną strukturę Map i końćzy program z kodem 0.
 * @param[in,out] map    – wskaźnik na strukturę przechowującą mapę dróg;
 * @return Wskaźnik na InputData z danymi o wczytanej linii.
 */
InputData* parseLine(Map* map);

#endif
